#include <Servo.h>
#include "FspTimer.h"

const int servoPin = 9;
const int startButtonPin = 2;
const int stopButtonPin = 4;
const int callbackFrequency = 10;  // 10Hz
const int timeToRotate = 600;      // 60s
const int minPulseWidth = 880;     // stopped
const int maxPulseWidth = 2080;    // rotating at max speed
const int pulseWidthStep = (maxPulseWidth - minPulseWidth) / 50;

enum Status {
  STOPPED,
  ACCELERATING,
  ROTATING_MAX_SPEED,
  DECELERATING,
};

Servo servo;
FspTimer fsp_timer;
Status status = STOPPED;
int currPulseWidth = minPulseWidth;

void setup() {
  initPins();
  initTimer();
}

void loop() {
}

void initPins() {
  servo.attach(servoPin, minPulseWidth, maxPulseWidth);
  pinMode(startButtonPin, INPUT_PULLUP);
  pinMode(stopButtonPin, INPUT_PULLUP);
  servo.writeMicroseconds(minPulseWidth);
}

void callback(timer_callback_args_t __attribute((unused)) * p_args) {
  switch (status) {
    case STOPPED:
      doAtStopped();
      break;
    case ACCELERATING:
      doAtAccelerating();
      break;
    case ROTATING_MAX_SPEED:
      doAtRotatingMaxSpeed();
      break;
    case DECELERATING:
      doAtDecelerating();
      break;
  }
}

void initTimer() {
  uint8_t type = GPT_TIMER;
  int8_t channel = FspTimer::get_available_timer(type);
  FspTimer::force_use_of_pwm_reserved_timer();
  fsp_timer.begin(TIMER_MODE_PERIODIC, type, channel, callbackFrequency, 0.0, callback);
  fsp_timer.setup_overflow_irq();
  fsp_timer.open();
  fsp_timer.start();
}

void doAtStopped() {
  servo.writeMicroseconds(minPulseWidth);

  int startButtonState = digitalRead(startButtonPin);
  if (startButtonState == LOW) {
    status = ACCELERATING;
  }
}

void doAtAccelerating() {
  if (currPulseWidth < maxPulseWidth) {
    currPulseWidth += pulseWidthStep;
  }
  servo.writeMicroseconds(currPulseWidth);

  if (currPulseWidth >= maxPulseWidth) {
    status = ROTATING_MAX_SPEED;
  }

  int stopButtonState = digitalRead(stopButtonPin);
  if (stopButtonState == LOW) {
    status = DECELERATING;
  }
}

void doAtRotatingMaxSpeed() {
  servo.writeMicroseconds(maxPulseWidth);

  static int count = 0;
  static bool isExit = false;
  count++;
  if (count >= timeToRotate) {
      isExit = true;
  }

  int stopButtonState = digitalRead(stopButtonPin);
  if (stopButtonState == LOW || isExit) {
    status = DECELERATING;
    count = 0;
    isExit = false;
  }
}

void doAtDecelerating() {
  if (currPulseWidth > minPulseWidth) {
    currPulseWidth -= pulseWidthStep;
  }
  servo.writeMicroseconds(currPulseWidth);

  if (currPulseWidth <= minPulseWidth) {
    status = STOPPED;
  }
}
